const authSettings = {
    rolesKey: "http://pweb-help-platform/role",
    phoneKey: "http://pweb-help-platform/phone_number",
    nameKey: "http://pweb-help-platform/full_name",
    addressKey: "http://pweb-help-platform/address",
    domain: "dev-xl3axr0e.us.auth0.com",
    clientId: "ZOIYCcqqQ9E02o4pZYWFaMPkA55QLU8J",
    audience: "pweb-help-platform"
  };
  
  export { authSettings };
  
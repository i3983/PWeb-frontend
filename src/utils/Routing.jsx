import React, { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import DonationsPostsRecipient from "../pages/recipient/DonationsPostsRecipient";
import RequirementsPostsDonor from "../pages/donor/NewPosts";
import { authSettings } from "../AuthSettings";
import MyAssingedPostsScreen from "../pages/donor/MyAssignedPosts";
import MyPostsScreen from "../pages/donor/MyPosts";
import AddNewPostScreen from "../pages/donor/AddNewPost";

const Router = () => {
  const { isAuthenticated, loginWithRedirect } = useAuth0();
  const { user } = useAuth0();

  useEffect(() => {
    if (!isAuthenticated) {
      loginWithRedirect();
    }
    console.log(user);
    console.log(user[authSettings.rolesKey]);
  }, [isAuthenticated, loginWithRedirect]);

  return (
    isAuthenticated && (
      <BrowserRouter>
        <Routes>
          {<Route exact path="/" element={<RequirementsPostsDonor />} />}
          {
            <Route
              exact
              path="/assignedPosts"
              element={<MyAssingedPostsScreen />}
            />
          }
          {<Route exact path="/myPosts" element={<MyPostsScreen />} />}
          {
            <Route
              exact
              path="/addNewDonation"
              element={<AddNewPostScreen />}
            />
          }
        </Routes>
      </BrowserRouter>
    )
  );
};

export default Router;

const colours = {
  blue01: "#D5DBF2",
  blue02: "#A5A6F6",
  blue03: "#0741AD",
  blue04: "#bad0fc",
  iris100: "#545CA4",
};

export { colours };

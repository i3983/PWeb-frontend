import logo from './logo.svg';
import './App.css';
import React from "react";
import Router from "./utils/Routing";
import Wrapper from "./utils/AuthWrapper";
import SideNav from './components/SideNav';

function App() {
  return (
    <Wrapper>
      <Router />
    </Wrapper>
  );
}

export default App;

import { Typography } from "@mui/material";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Link, useMatch, useResolvedPath } from "react-router-dom";
import { Grid } from "@mui/material";
import { Button } from "@mui/material";

const AccountComponent = ({ name, logout }) => {
  return (
    <div
      className="profile"
      style={{
        display: "flex",
        justifyContent: "flex-end",
        alignItems: "flex-end",
        marginRight: 20,
      }}
    >
      <div className="user-menu">
        <div
          className="profile-container"
          style={{
            display: "flex",
            gap: 4,
          }}
        >
          <div
            className="profile-info"
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Typography
              style={{
                fontWeight: 700,
              }}
            >
              {" "}
              {name}{" "}
            </Typography>{" "}
            <Link
              className="signout"
              to="/"
              onClick={() => {
                logout({
                  returnTo: window.location.origin,
                });
              }}
            >
              Signout{" "}
            </Link>{" "}
          </div>{" "}
          <AccountCircleIcon
            style={{
              fontSize: "50",
            }}
          />{" "}
        </div>{" "}
      </div>{" "}
    </div>
  );
};

export default AccountComponent;

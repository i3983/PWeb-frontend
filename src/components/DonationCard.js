import { Card, CardContent } from "@mui/material";
import { Typography, Divider } from "@mui/material";
import Button from "./Button";
import { Modal } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState, useEffect } from "react";
import { colours } from "../styling/colours";
import { config } from "../config";
import ItemsTable from "./DonationItemsTable";
import { MdOutlineClose } from "react-icons/md";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../AuthSettings";
import TableContainer from "@mui/material/TableContainer";
import { useNavigate } from "react-router-dom";

const styles = {
  cardStyle: {
    height: 300,
    width: 400,
    boxShadow: "5px 5px 10px rgba(0, 0, 0, 0.25)",
    borderRadius: "50px",
    backgroundColor: colours["blue01"],
  },
  cardContentStyle: {
    //textAlign: "center",
    p: 3,
  },
};

// let detailedDonation;

const DonationCard = ({ donation }) => {
  const { logout, user } = useAuth0();
  const [setError] = React.useState(null);
  const [open, setOpen] = useState(false);
  const [id, setId] = useState([]);
  const [detailedDonation, setDetailedDonation] = useState([]);
  const userType = user[authSettings.rolesKey];
  const isDonor = userType === "donor";
  const navigate = useNavigate();

  const getDonationById = async (donationId) => {
    let url = isDonor
      ? config.backendURL + `/requirement/${donationId}`
      : config.backendURL + `/donation/${donationId}`;
    console.log(url);
    try {
      const req = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const resp = await req.json();
      return resp;
    } catch (err) {}
  };

  const makeRequest = async () => {
    let url = config.backendURL + `/${userType}`;
    try {
      const req = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: isDonor
          ? JSON.stringify({
              donorEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedRequirements: false,
            })
          : JSON.stringify({
              recipientEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedRequirements: false,
            }),
      });
      const resp = await req.json();
      return resp;
    } catch (err) {
      setError(err);
    }
  };
  useEffect(() => {
    makeRequest().then((user) => setId(user.id));
  }, []);

  const assignToMe = async () => {
    console.log(donation.donorId);
    let url = isDonor
      ? config.backendURL + `/requirement/claim`
      : config.backendURL + `/donation/claim`;
    try {
      const req = await fetch(url, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: isDonor
          ? JSON.stringify({
              requirementId: donation.requirementId,
              donor: { id: id },
            })
          : JSON.stringify({
              donationId: donation.donationId,
              recipient: { id: id },
            }),
      });
      const resp = await req.json();
      setOpen(false);
      let path = "/assignedPosts";
      navigate(path);
      console.log(resp);
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  /*useEffect(() => {
    console.log("idDonor" + isDonor)
    console.log( donation.donationId)
    getDonationById(
      isDonor ? donation.requirementId : donation.donationId
    ).then(
      (donationResponse) => setDetailedDonation(donationResponse),
      console.log(detailedDonation.items)
    );
  }, []);*/

  // detailedDonation = getDonationById(donation.donationId);
  const handleOpen = () => {
    getDonationById(
      isDonor ? donation.requirementId : donation.donationId
    ).then(
      function(donationResponse) {
          setDetailedDonation(donationResponse);
          setOpen(true);
          console.log("Set open" + open)
    }
    )
  };

  const handleClose = () => {
    setOpen(false);
  };

  console.log(donation);

  return (
    <div>
      <Card sx={styles.cardStyle}>
        <CardContent sx={styles.cardContentStyle}>
          <div
            className="title"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <Typography style={{ fontWeight: 700, fontSize: 22 }}>
              {donation.postTitle}
            </Typography>
          </div>
          <div
            className="info-fields"
            style={{
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "space-between",
              margin: 20,
              display: "grid",
              columnGap: 30,
              rowGap: 20,
              gridTemplateColumns: "max-content max-content",
              width: "max-content",
            }}
          >
            <>
              <span className="field-key">
                <Typography
                  color={colours["iris100"]}
                  style={{ fontWeight: 700, fontSize: 17 }}
                >
                  {isDonor ? "Recipient:" : "Donator:"}
                </Typography>
              </span>
              <span>
                <Typography style={{ fontWeight: 700, fontSize: 17, width: 100 }}>
                  {isDonor ? donation.recipientName : donation.donorName}
                </Typography>
              </span>
            </>
            <>
              <span className="field-key">
                <Typography
                  color={colours["iris100"]}
                  style={{ fontWeight: 700, fontSize: 17 }}
                >
                  Publish date:
                </Typography>
              </span>
              <span>
                <Typography style={{ fontWeight: 700, fontSize: 17 }}>
                  {donation.publishDate}
                </Typography>
              </span>
            </>
            <>
              <span className="field-key">
                <Typography
                  color={colours["iris100"]}
                  style={{ fontWeight: 700, fontSize: 17 }}
                >
                  {isDonor
                    ? "No. of items requested:"
                    : "No. of items donated:"}
                </Typography>
              </span>
              <span>
                <Typography style={{ fontWeight: 700, fontSize: 17 }}>
                  {donation.numberItemsRequested} item(s)
                </Typography>
              </span>
            </>
          </div>
          <div
            className="sal"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              paddingTop: 20,
            }}
          >
            <Button
              variant="contained"
              onClick={() => {
                handleOpen();
              }}
            >
              Details
            </Button>
            <div
              style={{
                // display: "flex",
                alignItems: "center",
                justifyContent: "center",
                alignContent: "center",
                textAlign: "center",
              }}
            >
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box
                  backgroundColor="white"
                  style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    overflowY:"scroll",
                    transform: "translate(-50%, -50%)",
                  }}
                  // marginTop="10px"
                  // margin="200px"
                  // padding="200px"
                  height="600px"
                  width="600px"
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "flex-start",
                      justifyContent: "space-between",
                    }}
                  >
                    <Typography
                      style={{
                        fontWeight: 700,
                        fontSize: 25,
                        marginTop: 20,
                        marginLeft: 50,
                        marginBottom: 20,
                      }}
                      textAlign="left"
                    >
                      {donation.postTitle}
                    </Typography>

                    <MdOutlineClose
                      onClick={handleClose}
                      style={{
                        text: "black",
                        backgroundColor: "transparent",
                        stroke: "black",
                        height: "2em",
                        width: "2em",
                        marginTop: 20,
                        marginRight: 20,
                        marginBottom: 20,
                      }}
                    />
                  </div>
                  <Divider sx={{ borderBottomWidth: 5 }} />
                  <div
                    className="infoDonation"
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-start",
                      justifyContent: "space-between",
                      marginTop: 30,
                      marginBottom: 30,
                      marginLeft: 50,
                      display: "grid",
                      columnGap: 90,
                      rowGap: 20,
                      gridTemplateColumns: "max-content max-content",
                      width: "max-content",
                    }}
                  >
                    <>
                      <span className="field-key">
                        <Typography
                          color={colours["iris100"]}
                          style={{ fontWeight: 700, fontSize: 17 }}
                        >
                          {isDonor ? "Recipient:" : "Donor:"}
                        </Typography>
                      </span>
                      <span>
                        <Typography style={{ fontWeight: 700, fontSize: 17, width: 220 }}>
                          {isDonor
                            ? detailedDonation.recipientName
                            : detailedDonation.donorName}
                        </Typography>
                      </span>
                    </>
                    <>
                      <span className="field-key">
                        <Typography
                          color={colours["iris100"]}
                          style={{ fontWeight: 700, fontSize: 17 }}
                        >
                          {isDonor ? "Recipient phone number" : "Donor phone number:"}
                        </Typography>
                      </span>
                      <span>
                        <Typography style={{ fontWeight: 700, fontSize: 17, width: 220 }}>
                        {isDonor
                            ? detailedDonation.recipientPhoneNumber
                            : detailedDonation.donorPhoneNumber}
                        </Typography>
                      </span>
                    </>
                    <>
                      <span className="field-key">
                        <Typography
                          color={colours["iris100"]}
                          style={{ fontWeight: 700, fontSize: 17 }}
                        >
                          {isDonor ? "Recipient address:" : "Donor address:"}
                        </Typography>
                      </span>
                      <span>
                        <Typography style={{ fontWeight: 700, fontSize: 17, width: 220 }}>
                        {isDonor
                            ? detailedDonation.recipientAddress
                            : detailedDonation.donorAddress}
                        </Typography>
                      </span>
                    </>
                    <>
                      <span className="field-key">
                        <Typography
                          color={colours["iris100"]}
                          style={{ fontWeight: 700, fontSize: 17 }}
                        >
                          Publish date:
                        </Typography>
                      </span>
                      <span>
                        <Typography style={{ fontWeight: 700, fontSize: 17, width: 220 }}>
                          {detailedDonation.publishDate}
                        </Typography>
                      </span>
                    </>
                  </div>
                  <div>
                  <TableContainer
                    sx={{display: "flex", alignItems: "center", justifyContent: "center"}}
                  >
                    <ItemsTable items={detailedDonation.items} />
                  </TableContainer>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      paddingTop: 20,
                    }}
                  >
                    <Button
                      variant="contained"
                      onClick={() => {
                        assignToMe();
                      }}
                    >
                      Assign to me
                    </Button>
                  </div>
                </Box>
              </Modal>
            </div>
          </div>
        </CardContent>
      </Card>
    </div>
  );
};

export default DonationCard;

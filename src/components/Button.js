import React from "react";
import { colours } from "../styling/colours";
import { Typography } from "@mui/material";

const Button = ({ children, className, ...rest }) => {
  return (
    <button
      className="button"
      style={{
        borderRadius: "50px",
        background:
          rest.disabled === undefined
            ? colours["blue03"]
            : rest.disabled === false
            ? colours["blue03"]
            : "black",
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 5,
        paddingBottom: 5,
        border: 1,
        display: "flex",
        alignItems: "center",
      }}
      {...rest}
    >
      <Typography
        color="common.white"
        style={{
          fontWeight: 700,
          fontSize: 16,
        }}
      >
        {" "}
        {children}{" "}
      </Typography>{" "}
    </button>
  );
};

export default Button;

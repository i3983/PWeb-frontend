import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { colours } from "../styling/colours";
import { Typography, Divider } from "@mui/material";

function createData(itemName, quantity) {
  return { itemName, quantity };
}

export default function ItemsTable(items) {
  return (
      <Table
        sx={{
          width: 400,
          maxHeight: 200,
          backgroundColor: colours["blue04"],
          borderRadius: "30px",
        }}
        aria-label="simple table"
      >
        <TableHead sx={{ borderBottom: "#FFFF" }}>
          <TableRow>
            <TableCell
              sx={{ padding: "0px 50px", borderBottom: "2px solid #161418" }}
            >
              <Typography style={{ fontWeight: 700, fontSize: 17 }}>
                Article
              </Typography>
            </TableCell>
            <TableCell sx={{ borderBottom: "2px solid #161418" }}>
              <Typography style={{ fontWeight: 700, fontSize: 17 }}>
                Quantity
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.items.map((row) => (
            <TableRow
              key={row.itemName}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                sx={{ padding: "0px 50px", borderBottom: "1px solid #161418" }}
              >
                <Typography style={{ fontWeight: 700, fontSize: 15 }}>
                  {row.itemName}
                </Typography>
              </TableCell>
              <TableCell sx={{ borderBottom: "1px solid #161418" }}>
                <Typography style={{ fontWeight: 700, fontSize: 15 }}>
                  {row.quantity}
                </Typography>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
  );
}

import { Card, CardActionArea, Drawer, CardContent } from "@mui/material";
import React from "react";
import { Grid, Item, List, Toolbar, Divider, Box } from "@mui/material";
import { Typography } from "@mui/material";
import { colours } from "../styling/colours";
import { textAlign } from "@mui/system";
import logo from "../assets/logo.png";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../AuthSettings";

const drawerWidth = 200;

const styles = {
  cardStyle: {
    borderRadius: "50px",
    height: 80,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0.5,
    marginBottom: 0.5,
    textAlign: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colours["blue02"],
  },
  cardActionStyle: {
    height: 80,
  },
  cardContentStyle: {
    textAlign: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    pt: 3.3,
  },
  column: {},
};

const SideNav = ({ children }) => {
  const { logout, user } = useAuth0();
  const userType = user[authSettings.rolesKey];
  const isDonor = userType === "donor";
  const list = isDonor
    ? ["New requirements", "My donor posts", "My assigned requirements"]
    : ["New donations", "My requirement posts", "My assigned donations"];
  return (
    <div className="menu" style={{ display: "flex" }}>
      <div>
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              backgroundColor: colours["blue01"],
              width: drawerWidth,
            },
          }}
          variant="permanent"
          anchor="left"
        >
          <div
            className="logo-container"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img src={logo} alt="Weblib logo" />
          </div>
          <List sx={styles.column}>
            {list.map((text) => (
              <Card sx={styles.cardStyle}>
                <CardActionArea
                  sx={styles.cardActionStyle}
                  href={
                    { text }.text === "My donor posts" ||
                    { text }.text === "My requirement posts"
                      ? "/myPosts"
                      : { text }.text === "My assigned requirements" ||
                        { text }.text === "My assigned donations"
                      ? "/assignedPosts"
                      : "/"
                  }
                >
                  <CardContent sx={styles.cardContentStyle}>
                    <Typography style={{ fontWeight: 700 }}>{text}</Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))}
          </List>
        </Drawer>
      </div>
      <div
        className="content"
        style={{
          marginLeft: 350,
          position: "absolute",
          top: "0px",
          right: "0px",
          bottom: "0px",
          left: "0px",
        }}
      >
        {children}
      </div>
    </div>
  );
};
export default SideNav;

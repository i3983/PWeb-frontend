export class Donation {
    constructor(postTitle, donorId, publishDate) {
        this.postTitle = postTitle;
        this.donorId = donorId;
        this.publishDate = publishDate;
    }
}
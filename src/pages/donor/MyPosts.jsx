import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../../AuthSettings";
import { config } from "../../config";
import SideNav from "../../components/SideNav";
import MyDonationCard from "../../components/MyDonationCard";
import AccountComponent from "../../components/AccountComponent";
import Button from "../../components/Button";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import { Typography } from "@mui/material";

const MyPostsScreen = () => {
  const { logout, user } = useAuth0();
  const [setError] = React.useState(null);
  const [donationsArray, setDonationsArray] = useState([]);
  const [id, setId] = useState([]);
  const userType = user[authSettings.rolesKey];
  const isDonor = userType === "donor";

  const makeRequest = async () => {
    let url = config.backendURL + `/${userType}`;
    try {
      const req = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: isDonor
          ? JSON.stringify({
              donorEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedRequirements: false,
            })
          : JSON.stringify({
              recipientEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedRequirements: false,
            }),
      });
      const resp = await req.json();
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  const getMyDonations = async (userId) => {
    let url = isDonor
      ? config.backendURL + `/donation/donor/${userId}`
      : config.backendURL + `/requirement/recipient/${userId}`;
    console.log(url);
    try {
      const req = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const resp = await req.json();
      console.log(resp);
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    makeRequest().then((user) => {
      setId(user.id);
      getMyDonations(user.id).then((array) => {
        setDonationsArray(array);
        console.log(array);
      });
    });
  }, []);

  const name = user[authSettings.nameKey];
  const MyPosts = () => {
    console.log(id);
    return (
      <div className="layout">
        <AccountComponent name={name} logout={logout} />
        <div
          className="header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginRight: 20,
            marginTop: 10,
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: 25 }}>
            {isDonor ? "My Donations" : "My requirements"}
          </Typography>

          <Link to={"/addNewDonation"} state={{ id: id }}>
            <Button>
              <FiPlus size={20} viewBox="0 0 25 15" />{" "}
              {isDonor ? "Add new donation post" : "Add new requirement post"}
            </Button>
          </Link>
        </div>
        <div
          className="donations"
          style={{
            display: "flex",
            flexWrap: "wrap",
            gap: 150,
            marginTop: 50,
            paddingBottom: 60,
          }}
        >
          {donationsArray.length > 0 ? (
            donationsArray.map((donation) => (
              <MyDonationCard donation={donation} />
            ))
          ) : (
            <div></div>
          )}
        </div>
      </div>
    );
  };

  return (
    <div>
      <SideNav children={<MyPosts />} />
    </div>
  );
};
export default MyPostsScreen;

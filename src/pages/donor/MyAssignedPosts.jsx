import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../../AuthSettings";
import { config } from "../../config";
import SideNav from "../../components/SideNav";
import AssignedDonationCard from "../../components/AssignedDonationCard";
import AccountComponent from "../../components/AccountComponent";
import Button from "../../components/Button";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import { Typography } from "@mui/material";

const MyAssignedPostsScreen = () => {
  const { logout, user } = useAuth0();
  const [setError] = React.useState(null);
  const [donationsArray, setDonationsArray] = useState([]);
  const [id, setId] = useState([]);
  const userType = user[authSettings.rolesKey];
  const isDonor = userType === "donor";

  const makeRequest = async () => {
    let url = config.backendURL + `/${userType}`;
    try {
      const req = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body:
          userType === "donor"
            ? JSON.stringify({
                donorEmail: user.email,
                phoneNumber: user[authSettings.phoneKey],
                address: user[authSettings.addressKey],
                name: user[authSettings.nameKey],
                subscribedRequirements: false,
              })
            : JSON.stringify({
                recipientEmail: user.email,
                phoneNumber: user[authSettings.phoneKey],
                address: user[authSettings.addressKey],
                name: user[authSettings.nameKey],
                subscribedRequirements: false,
              }),
      });
      const resp = await req.json();
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  const getAssignedRequirements = async (userId) => {
    let url = isDonor
      ? config.backendURL + `/requirement/donor/${userId}`
      : config.backendURL + `/donation/recipient/${userId}`;
    console.log(url);
    try {
      const req = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const resp = await req.json();
      console.log(resp);
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    makeRequest().then((user) => {
      setId(user.id);
      getAssignedRequirements(user.id).then((array) => {
        setDonationsArray(array);
        console.log(array);
      });
    });
  }, []);

  const name = user[authSettings.nameKey];
  const MyAssignedPosts = () => {
    console.log(id);
    return (
      <div className="layout">
        <AccountComponent name={name} logout={logout} />
        <div
          className="header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginRight: 20,
            marginTop: 10,
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: 25 }}>
            {isDonor
              ? "Requirements assigned to me"
              : "Donations assigned to me"}
          </Typography>
        </div>
        <div
          className="donations"
          style={{
            display: "flex",
            flexWrap: "wrap",
            gap: 150,
            marginTop: 50,
            paddingBottom: 60,
          }}
        >
          {donationsArray.length > 0 ? (
            donationsArray.map((donation) => (
              <AssignedDonationCard donation={donation} />
            ))
          ) : (
            <div></div>
          )}
        </div>
      </div>
    );
  };

  return (
    <div>
      <SideNav children={<MyAssignedPosts />} />
    </div>
  );
};
export default MyAssignedPostsScreen;

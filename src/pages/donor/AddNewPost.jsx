import React, { useState } from "react";
import SideNav from "../../components/SideNav";
import { useLocation } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../../AuthSettings";
import AccountComponent from "../../components/AccountComponent";
import { Typography, TextField } from "@mui/material";
import Button from "../../components/Button";
import ItemsTable from "../../components/DonationItemsTable";
import TableContainer from "@mui/material/TableContainer";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import { config } from "../../config";

const AddNewPostScreen = () => {
  const location = useLocation();
  const params = location.state;
  const navigate = useNavigate()

  const { logout, user } = useAuth0();
  // console.log(user);
  const name = user[authSettings.nameKey];

  // const { logout } = location.state.logout;
  // console.log(params.id);

  const AddNewPost = () => {
    const [title, setTitle] = useState("");
    const [titleError, setTitleError] = useState("");
    const [articleName, setArticleName] = useState("");
    const [quantity, setQuantity] = useState("");
    const [items, setItems] = useState([]);
    const [setError] = React.useState(null);
    const userType = user[authSettings.rolesKey];
    const isDonor = userType === "donor";

    const submitDonation = async () => {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, "0");
      var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
      var yyyy = today.getFullYear();
      today = yyyy + "-" + mm + "-" + dd;
      let url = isDonor
        ? config.backendURL + "/donation"
        : config.backendURL + "/requirement";
      try {
        const req = await fetch(url, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: isDonor
            ? JSON.stringify({
                postTitle: title,
                publishDate: today,
                status: "ACTIVE",
                items: items,
                donor: { id: params.id },
              })
            : JSON.stringify({
                postTitle: title,
                publishDate: today,
                status: "ACTIVE",
                items: items,
                recipient: { id: params.id },
              }),
        });
        console.log(
          JSON.stringify({
            postTitle: title,
            publishDate: today,
            status: "ACTIVE",
            items: items,
            donor: params.id,
          })
        );
        const resp = await req.json();
        console.log(resp);
      } catch (err) {
        setError(err);
      }
      let path = "/myPosts";
      navigate(path);
    };

    const handleTitleChange = (e) => {
      setTitle(e.target.value);
      if (e.target.value === "") {
        setTitleError("This field cannot be empty!");
      } else {
        setTitleError("");
      }
    };

    const handleNameChange = (e) => {
      setArticleName(e.target.value);
    };

    const handleQuantityChange = (e) => {
      setQuantity(e.target.value);
    };

    const addArticle = () => {
      var item = { itemName: articleName, quantity: quantity };
      setItems((items) => [...items, item]);
    };

    return (
      <div className="layout">
        <AccountComponent name={name} logout={logout} />
        <div
          className="header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginRight: 20,
            marginTop: 10,
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: 25 }}>
            {isDonor ? "Add Donation Post" : "Add Requirement Post"}
          </Typography>
        </div>
        <div
          className="flex flex-col gap-10"
          style={{
            display: "flex",
            flexDirection: "column",
            width: "800px",
            marginTop: "40px",
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: 18 }}>
            Post title:
          </Typography>
          <TextField
            id="outlined-basic"
            variant="outlined"
            InputProps={{ style: { fontSize: 18, fontWeight: 700, borderRadius: "20px" } }}
            InputLabelProps={{ style: { fontSize: 18, fontWeight: 700} }}
            onChange={handleTitleChange}
            error={titleError !== ""}
          />
          <Typography style={{ fontWeight: 700, fontSize: 18, marginTop: 20}}>
            Article name:
          </Typography>
          <TextField
            id="outlined-basic"
            variant="outlined"
            InputProps={{ style: { fontSize: 18, fontWeight: 700, borderRadius: "20px" } }}
            InputLabelProps={{ style: { fontSize: 18, fontWeight: 700} }}
            onChange={handleNameChange}
          />
          <Typography style={{ fontWeight: 700, fontSize: 18, marginTop: 20 }}>
            Quantity:
          </Typography>
          <TextField
            id="outlined-basic"
            variant="outlined"
            InputProps={{ style: { fontSize: 18, fontWeight: 700, borderRadius: "20px" } }}
            InputLabelProps={{ style: { fontSize: 18, fontWeight: 700} }}
            sx={{ mb: 4 }}
            onChange={handleQuantityChange}
          />
        </div>
        <div style={{ paddingBottom: 8 }}>
          <Button
            onClick={addArticle}
            disabled={articleName === "" || quantity === ""}
          >
            Add Article
          </Button>
        </div>
        <div>
          {items.length ?
          <TableContainer
          sx={{display: "flex", alignItems: "center", marginTop: 5, marginBottom: 5}}
          >
          <ItemsTable items={items}/>
          </TableContainer> : <div></div>}
        </div>
        <Button
          onClick={submitDonation}
          disabled={title === "" || !items.length}
        >
          Finish
        </Button>
      </div>
    );
  };

  return (
    <div>
      <SideNav children={<AddNewPost />} />
    </div>
  );
};
export default AddNewPostScreen;

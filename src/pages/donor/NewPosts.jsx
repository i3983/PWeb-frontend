import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { authSettings } from "../../AuthSettings";
import { config } from "../../config";
import SideNav from "../../components/SideNav";
import DonationCard from "../../components/DonationCard";
import AccountComponent from "../../components/AccountComponent";
import Button from "../../components/Button";
import { FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import { Typography } from "@mui/material";

const NewPostsScreen = () => {
  const { logout, user } = useAuth0();
  const [setError] = React.useState(null);
  const [donationsArray, setDonationsArray] = useState([]);
  const [id, setId] = useState([]);
  const [subscribed, setSubscribed] = useState([]);
  const [buttonText, setButtonText] = useState("Subscribe to newsletter");
  const userType = user[authSettings.rolesKey];
  const isDonor = userType === "donor";

  const makeRequest = async () => {
    console.log(user.email);
    console.log(user[authSettings.nameKey]);
    console.log(user[authSettings.phoneKey]);
    console.log(user[authSettings.addressKey]);
    let url = config.backendURL + `/${userType}`;
    console.log(url);
    try {
      const req = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: isDonor
          ? JSON.stringify({
              donorEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedRequirements: false,
            })
          : JSON.stringify({
              recipientEmail: user.email,
              phoneNumber: user[authSettings.phoneKey],
              address: user[authSettings.addressKey],
              name: user[authSettings.nameKey],
              subscribedDonations: false,
            }),
      });
      const resp = await req.json();
      return resp;
    } catch (err) {
      setError(err);
    }
  };
  useEffect(() => {
    makeRequest().then(
      (user) => setId(user.id),
      isDonor
        ? setSubscribed(user.subscribedRequirements)
        : setSubscribed(user.subscribedDonations)
    );
  }, []);

  const getDonations = async () => {
    let url = isDonor
      ? config.backendURL + "/requirement/active"
      : config.backendURL + "/donation/active";
    try {
      const req = await fetch(url, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const resp = await req.json();
      console.log(resp);
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    getDonations().then((array) => {
      console.log(array);
      setDonationsArray(array);
    });
  }, []);

  const handleSubscribe = async () => {
    console.log(user);
    let url = isDonor
      ? subscribed
        ? config.backendURL + `/donor/unsubscribe/${id}`
        : config.backendURL + `/donor/subscribe/${id}`
      : subscribed
      ? config.backendURL + `/recipient/unsubscribe/${id}`
      : config.backendURL + `/recipient/subscribe/${id}`;
    try {
      const req = await fetch(url, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });
      const resp = await req.json();
      console.log(resp);
      return resp;
    } catch (err) {
      setError(err);
    }
  };

  // const donation = new Donation("Clothes for women", "1", "22/03/2022");
  const name = user[authSettings.nameKey];
  const NewPosts = () => {
    // console.log(donationsArray[0]);
    // console.log(id);
    // console.log(user);
    return (
      <div className="layout">
        <AccountComponent name={name} logout={logout} />
        <div
          className="header"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            marginRight: 20,
            marginTop: 10,
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: 25 }}>
            {isDonor ? "Latest requirement posts" : "Latest donation posts"}
          </Typography>

          <Button
            onClick={() => {
              handleSubscribe()
                .then((user) =>
                  isDonor
                    ? setSubscribed(user.subscribedRequirements)
                    : setSubscribed(user.subscribedDonations)
                )
                .then(() =>
                  subscribed
                    ? setButtonText("Subscribe to newsletter")
                    : setButtonText("Unsubscribe from newsletter")
                );
            }}
          >
            {buttonText}
          </Button>
        </div>
        <div
          className="donations"
          style={{
            display: "flex",
            flexWrap: "wrap",
            gap: 150,
            marginTop: 50,
            paddingBottom: 60,
          }}
        >
          {donationsArray.map((donation) => (
            <DonationCard donation={donation} />
          ))}
        </div>
      </div>
    );
  };

  return (
    <div>
      <SideNav children={<NewPosts />} />
    </div>
  );
};

export default NewPostsScreen;

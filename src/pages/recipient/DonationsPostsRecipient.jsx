import React, { useState, useMemo, useEffect } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import {Link} from 'react-router-dom';
import { authSettings } from "../../AuthSettings";
import { config } from "../../config";

const DonationsPostsRecipient = () => {
    const { logout, user } = useAuth0();
    const [error, setError] = React.useState(null);

    useEffect(() => {
        const makeRequest = async () => {
            console.log(user.email);
            console.log(user[authSettings.phoneKey]);
            console.log(user[authSettings.addressKey]);
            let url = config.backendURL + "/recipient";
            console.log(url);
          try {
            const req = await fetch(url, {
                method: 'POST',
                headers: {
                  "Accept": "application/json",
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  email: user.email,
                  phoneNumber: user[authSettings.phoneKey],
                  address: user[authSettings.addressKey],
                  name: user[authSettings.nameKey],
                  subscribedDonations: false
                })
            });
            const resp = await req.json();
            console.log(resp);
          } catch (err) {
            setError(err);
          }
        };
    
        makeRequest();
      }, []);
    
    return (
      <div>
        <h2>RECIPIENT PAGE</h2>
        <div className="flex flex-col gap-10">
                <Link
                className="signout"
                to="/"
                onClick={() => {
                logout({ returnTo: window.location.origin });
                }}
            >
                Signout
            </Link>
        </div>
      </div>
    );
};

export default DonationsPostsRecipient;